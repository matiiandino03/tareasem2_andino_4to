using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootWater : MonoBehaviour
{
    public bool canShoot = false;
    public GameObject waterParticle;
    public Transform fireTransform;

    private void Update()
    {
        if(Input.GetMouseButtonDown(0)&&canShoot)
        {
            Shoot();
        }
    }

    public void Shoot()
    {
        GameObject bullet = Instantiate(waterParticle, fireTransform.position, fireTransform.rotation,null);
        foreach(Rigidbody rb in bullet.GetComponentsInChildren<Rigidbody>())
        {
            rb.AddForce(bullet.transform.forward * 15, ForceMode.Impulse);
        }
    }
}
