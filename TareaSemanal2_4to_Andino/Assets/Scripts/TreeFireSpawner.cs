using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeFireSpawner : MonoBehaviour
{
    public List<Transform> spawns = new List<Transform>();
    public List<GameObject> firesAlive = new List<GameObject>();
    public GameObject fire;
    public Material Burned;
    void Start()
    {
        int rand = Random.Range(0, 1);
        if(rand == 1)
        {
            SpawnFire();
        }
        else
        {
            SpawnFire();
            SpawnFire();
        }
        StartCoroutine("SpawnAfterTime");
    }


    public void SpawnFire()
    {
        if(spawns.Count == 0 || spawns[0] == null)
        {
            GetComponent<Renderer>().material = Burned;
            GameManager.instance.BurnedTrees++;
            foreach(GameObject g in firesAlive)
            {
                if(g != null)
                {
                    g.GetComponent<FireWaterDetection>().OnlySmoke();
                }
            }
            return;
        }
        int rand = Random.Range(0, spawns.Count);
        GameObject newFire = Instantiate(fire, spawns[rand].position, spawns[rand].rotation);
        spawns.Remove(spawns[rand]);
        firesAlive.Add(newFire);
        GameManager.instance.firesAlive ++;
    }

    IEnumerator SpawnAfterTime()
    {
        int rand = Random.Range(50, 120);
        yield return new WaitForSeconds(rand);
        foreach(GameObject g in firesAlive)
        {
            if(g != null)
            {
                SpawnFire();
                StartCoroutine("SpawnAfterTime");
                yield break;
            }
        }
    }
}
