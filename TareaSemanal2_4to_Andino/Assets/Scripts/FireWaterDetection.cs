using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireWaterDetection : MonoBehaviour
{
    public ParticleSystem fire1;
    public ParticleSystem fire2;
    public ParticleSystem smoke1;
    public ParticleSystem smoke2;
    int index = 0;
    private void Start()
    {
        fire1.Play();
        fire2.Pause();
        smoke1.Play();
        smoke2.Pause();
    }

    private void OnTriggerEnter(Collider other)
    {

        if(other.tag == "Water")
        {
            if(index == 0)
            {
                fire2.Play();
                fire1.Stop();
                smoke2.Play();
                smoke1.Stop();
                index = 1;
                Destroy(other.gameObject);
                return;
            }
            else if(index > 0)
            {
                Destroy(this.gameObject);
                GameManager.instance.firesAlive--;
            }
        }
    }

    public void OnlySmoke()
    {
        fire2.Stop();
        fire1.Stop();
        smoke2.Play();
        smoke1.Play();
    }
}
