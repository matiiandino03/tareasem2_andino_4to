using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public int BurnedTrees = 0;
    public int firesAlive = 0;
    public GameObject PanelBurnedForest;
    public Text burnedTrees;
    public GameObject win;
    public Text firesAliveTxt;
    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this);
        }
        else
        {
            instance = this;
        }
    }

    private void Update()
    {
        burnedTrees.text = BurnedTrees.ToString();
        firesAliveTxt.text = firesAlive.ToString();
        if (BurnedTrees >= 3)
        {
            PanelBurnedForest.SetActive(true);
            StartCoroutine("EndGameLost");
        }
        if(BurnedTrees < 3 && firesAlive == 0)
        {
            StartCoroutine("EndGameWin");
        }
    }

    IEnumerator EndGameLost()
    {
        PanelBurnedForest.SetActive(true);
        yield return new WaitForSeconds(3);
        SceneManager.LoadScene("Level0");
    }
    IEnumerator EndGameWin()
    {
        win.SetActive(true);
        yield return new WaitForSeconds(3);
        SceneManager.LoadScene("Level1");
    }
}
