using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabPeople : MonoBehaviour
{
    public List<GameObject> people = new List<GameObject>();
    GameObject closestPerson = null;
    bool isPicking = false;
    public Transform peoplePos;

    void Update()
    {      
        foreach(GameObject g in people)
        {
            if(closestPerson == null)
            {
                closestPerson = g;
            }
            else
            {
                if(Vector3.Distance(transform.position, closestPerson.transform.position) > Vector3.Distance(transform.position, g.transform.position))
                {
                    closestPerson = g;
                }
            }
        }
        if (Input.GetKeyDown(KeyCode.E) && isPicking)
        {
            EndPickUp();
            return;
        }
        if (Input.GetKeyDown(KeyCode.E) && !isPicking)
        {
            if (Vector3.Distance(transform.position, closestPerson.transform.position) < 3.5f)
            {
                StartPickUp();
                return;
            }
        }
    }

    public void StartPickUp()
    {
        closestPerson.GetComponent<Rigidbody>().isKinematic = true;
        closestPerson.transform.SetParent(peoplePos);
        closestPerson.transform.position = peoplePos.position;
        closestPerson.transform.rotation = peoplePos.rotation;
        isPicking = true;
    }

    public void EndPickUp()
    {
        isPicking = false;
        closestPerson.transform.SetParent(null);
        closestPerson.GetComponent<Rigidbody>().isKinematic = false;
    }
}
