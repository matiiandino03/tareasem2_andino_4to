using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterEvaporate : MonoBehaviour
{
    void Start()
    {
        StartCoroutine("StartEvaporate");
    }

    IEnumerator StartEvaporate()
    {
        yield return new WaitForSeconds(5);
        foreach(SphereCollider s in GetComponentsInChildren<SphereCollider>())
        {
            s.enabled = false;
        }
        yield return new WaitForSeconds(0.5f);
        Destroy(this.gameObject);
    }


}
