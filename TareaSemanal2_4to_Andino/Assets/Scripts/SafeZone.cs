using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SafeZone : MonoBehaviour
{
    int peopleCount = 0;
    public Text txtPeopleSaved;
    public ParticleSystem ps;
    public GrabPeople gp;
    public bool noFires = false;

    public GameObject win;

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Person"))
        {
            Destroy(other.gameObject);
            StartCoroutine("StopParticles");
            gp.people.Remove(other.gameObject);
            peopleCount++;
        }
    }

    private void Update()
    {
        txtPeopleSaved.text = "People Saved: " + peopleCount + "/3";
        if(noFires && peopleCount == 3)
        {
            StartCoroutine("EndGame");
        }
    }

    IEnumerator StopParticles()
    {
        ps.Play();
        yield return new WaitForSeconds(1);
        ps.Stop();
    }

    IEnumerator EndGame()
    {
        win.SetActive(true);
        yield return new WaitForSeconds(3);
        SceneManager.LoadScene("Menu");
    }
}
