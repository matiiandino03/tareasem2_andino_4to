using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabLadder : MonoBehaviour
{
    public GameObject ladder;
    bool isPicking = false;
    public Transform ladderPos;

    void Update()
    {
        if(Vector3.Distance(transform.position, ladder.transform.position) > 5f && GetComponent<PlayerController>().isClimbing == true)
        {
            GetComponent<PlayerController>().isClimbing = false;
            transform.rotation = Quaternion.identity;
        }
        if (Input.GetKeyDown(KeyCode.F) && !isPicking)
        {
            if (Vector3.Distance(transform.position, ladder.transform.position) < 3.5f && GetComponent<PlayerController>().isClimbing == false)
            {
                GetComponent<PlayerController>().isClimbing = true;
            }
            else if (GetComponent<PlayerController>().isClimbing == true)
            {
                GetComponent<PlayerController>().isClimbing = false;
                transform.rotation = Quaternion.identity;
            }
        }
        if (Input.GetKeyDown(KeyCode.E) && isPicking)
        {
            EndPickUp();
            return;
        }
        if (Input.GetKeyDown(KeyCode.E) && !isPicking)
        {
            if (Vector3.Distance(transform.position, ladder.transform.position) < 3.5f)
            {
                StartPickUp();
                return;
            }
        }        
    }

    public void StartPickUp()
    {
        ladder.GetComponent<Rigidbody>().isKinematic = true;
        ladder.transform.SetParent(ladderPos);
        ladder.transform.position = ladderPos.position;
        ladder.transform.rotation = ladderPos.rotation;
        isPicking = true;
    }

    public void EndPickUp()
    {
        isPicking = false;
        ladder.transform.SetParent(null);
        ladder.GetComponent<Rigidbody>().isKinematic = false;
    }
}
