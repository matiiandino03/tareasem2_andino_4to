using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FireSpawner : MonoBehaviour
{
    public List<Transform> fireSpawns = new List<Transform>();
    public GameObject Flames;
    List<GameObject> firesAlive = new List<GameObject>();
    public List<GameObject> BlockStairs = new List<GameObject>();
    public Text txtFires;
    public SafeZone sz;
    private void Start()
    {
        SpawnFire();
        SpawnFire();
        SpawnFire();
        SpawnFire();
        SpawnFire();
        SpawnFire();
        SpawnFire();
        SpawnFire();
        SpawnFire();
        SpawnFire();
        SpawnFire();
        SpawnFire();
        SpawnFire();
        SpawnFire();
        SpawnFire();
        StartCoroutine("SpawnAfterTime");
        StartCoroutine("blockStairs");
    }
    private void Update()
    {
        foreach(GameObject g in firesAlive)
        {
            if(g == null)
            {
                firesAlive.Remove(g);
                break;
            }
        }
        if(firesAlive.Count == 0)
        {
            sz.noFires = true;
        }
        txtFires.text = firesAlive.Count.ToString();
    }
    public void SpawnFire()
    {
        if (fireSpawns.Count == 0 || fireSpawns[0] == null)
        {
            foreach (GameObject g in firesAlive)
            {
                if (g != null)
                {
                    g.GetComponent<FireWaterDetection>().OnlySmoke();
                }
            }
            return;
        }
        int rand = Random.Range(0, fireSpawns.Count);
        GameObject newFire = Instantiate(Flames, fireSpawns[rand].position, fireSpawns[rand].rotation);
        fireSpawns.Remove(fireSpawns[rand]);
        firesAlive.Add(newFire);
    }

    IEnumerator SpawnAfterTime()
    {
        int rand = Random.Range(15, 30);
        yield return new WaitForSeconds(rand);
        foreach (GameObject g in firesAlive)
        {
            if (g != null)
            {
                SpawnFire();
                SpawnFire();
                SpawnFire();
                StartCoroutine("SpawnAfterTime");
                yield break;
            }
        }
    }

    IEnumerator blockStairs()
    {
        yield return new WaitForSeconds(60);
        foreach (GameObject g in BlockStairs)
        {
            g.SetActive(true);
        }
    }
}
