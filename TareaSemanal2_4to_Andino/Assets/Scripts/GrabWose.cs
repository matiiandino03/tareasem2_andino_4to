using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabWose : MonoBehaviour
{
    public GameObject woseEnd;
    bool isPicking = false;
    public Transform playerHand;
    public Transform lastParent;
    public Transform woseStart;
    public ShootWater sw;
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E) && isPicking)
        {
            EndPickUp();
            return;
        }
        if (Input.GetKeyDown(KeyCode.E) && !isPicking)
        {
            if (Vector3.Distance(transform.position, woseEnd.transform.position) < 2)
            {
                StartPickUp();
                return;
            }
        }
        if(Vector3.Distance(woseStart.transform.position, woseEnd.transform.position) > 180)
        {
            EndPickUp();
            woseEnd.transform.LookAt(woseStart.position);
            woseEnd.GetComponent<Rigidbody>().AddForce(woseEnd.transform.forward*3,ForceMode.Impulse);
        }
    }

    public void StartPickUp()
    {
        woseEnd.GetComponent<Rigidbody>().isKinematic = true;
        woseEnd.transform.SetParent(playerHand);
        woseEnd.transform.position = playerHand.position;
        woseEnd.transform.rotation = playerHand.rotation;
        isPicking = true;
        sw.canShoot = true;
    }

    public void EndPickUp()
    {
        isPicking = false;
        sw.canShoot = false;
        woseEnd.transform.SetParent(lastParent);
        woseEnd.GetComponent<Rigidbody>().isKinematic = false;
    }
}
