using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float rapidezDesplazamiento = 5.0f;
    public float rapidezDesplazamientoSprint = 5.0f;
    public GameObject Jugador;
    public GameObject CheckGround;
    public LayerMask mask;
    public Rigidbody rb;
    public bool isClimbing = false;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {       
        if(isClimbing)
        {
            Climbing();
        }
        else
        {
            Move();
        }      

    }

    public void Move()
    {
        rb.isKinematic = false;
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            rapidezDesplazamiento += rapidezDesplazamientoSprint;
        }
        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            rapidezDesplazamiento = 5.0f;
        }

        float movimientoAdelanteAtras = Input.GetAxis("Vertical") * rapidezDesplazamiento;
        float movimientoCostados = Input.GetAxis("Horizontal") * rapidezDesplazamiento;

        movimientoAdelanteAtras *= Time.deltaTime;
        movimientoCostados *= Time.deltaTime;


        transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras);

        if (Input.GetKeyDown(KeyCode.Space) && Physics.OverlapSphere(CheckGround.transform.position, 1, mask).Length > 0)
        {
            GetComponent<Rigidbody>().AddForce(Vector3.up * 800);
        }
    }

    public void Climbing()
    {
        GameObject ladder = GameObject.FindGameObjectWithTag("Ladder");
        transform.rotation = ladder.transform.rotation;

        float movimientoAdelanteAtras = Input.GetAxis("Vertical") * rapidezDesplazamiento;

        movimientoAdelanteAtras *= Time.deltaTime;

        rb.isKinematic = true;

        transform.Translate(0, movimientoAdelanteAtras, 0);

    }

}
